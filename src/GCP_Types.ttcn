///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:         GCP_Types.ttcn
//  Description:  GCP Enc
//  Rev:		  <RnXnn>
//  Prodnr:             CNL 113 364
//  Updated:            2012-11-21
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module GCP_Types
{

import from GCP_PDU_Defs language "ASN.1:1994" all;

external function enc_PDU_GCP(in MegacoMessage pdu) return octetstring
 with { extension "prototype(convert) encode(BER:BER_ENCODE_DER)" };
external function dec_PDU_GCP(in octetstring stream) return MegacoMessage
 with { extension "prototype(convert) decode(BER:BER_ACCEPT_ALL)" };

external function enc_IA5String(in SDP_IA5String pdu) return octetstring
 with { extension "prototype(convert) encode(BER:BER_ENCODE_DER)" };
external function dec_IA5String(in octetstring stream) return SDP_IA5String
 with { extension "prototype(convert) decode(BER:BER_ACCEPT_ALL)" };

} with {
  extension "version <RnXnn>"
}

